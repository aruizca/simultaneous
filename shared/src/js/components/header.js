import React, { PropTypes, Component } from "react";

class Header extends Component {

    render() {
        return (
            <header className="aui-page-header">
                <div className="aui-page-header-inner">
                    <div className="aui-page-header-main intro-header">
                        <h1>Issue Grid</h1>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;