import request from "jira-client";

function doJqlSearch(jql) {
    return request(`/rest/api/2/search?jql=${jql}`);
}

export function fetchIssues(jql) {
    return doJqlSearch(jql).then(response => response.issues);
}

export function fetchInitData(projectKey) {
    return fetchIssues(`project = ${projectKey}`);
}
