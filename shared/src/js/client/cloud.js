import Promise from "promise";

function handleResponse(response) {
    if (response.status !== 200) {
        return new Error(`Error: statusCode ${response.status}`);
    }

    return response.json();
}

function handleError(error) {
    console.error(error);
}

export default function request(url, body, method = "GET", headers = {
    "Content-Type": "application/json",
    "Accept": "application/json"
}) {
    return new Promise((resolve, reject) => {
        window.AP.require("request", request => {

            var options = {
                url: url,
                type: method,
                success: (response) => resolve(JSON.parse(response)),
                error: reject
            };

            if (body) {
                options.data = JSON.stringify(body);
                options.contentType = headers["Content-Type"];
            }

            request(options)
        });
    });
}