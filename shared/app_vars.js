import path from "path"
export const src_root = path.join(__dirname, "src", "js");
export const less_root = path.join(__dirname, "src", "less");
export const containers_src = path.join(src_root, "containers");
