import path from "path";
import webpack from 'webpack';
import baseConfig, { src_root, containers_src } from "./baseWebpackConfig";
import _ from "lodash";

function getConfig(mode) {
    var config = _.cloneDeep(baseConfig);

    config.entry = {};
    config.entry[mode] = [
        'babel-polyfill',
        'webpack-dev-server/client?http://localhost:3001',
        'webpack/hot/only-dev-server',
        `./src/js/jira-${mode}`
    ];

    config.resolve.alias["jira-client"] = path.join(src_root, "client", `${mode}.js`);

    return config;
}

module.exports = [getConfig("cloud"), getConfig("server")];